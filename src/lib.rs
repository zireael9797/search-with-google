//! # Search With Google
//! ## Usage
//!
//! ```rust
//! use search_with_google::search;
//!
//! # async fn run() {
//!     let results = search("rust", 3, None).await;
//!     if let Ok(result_list) = results{
//!         println!("Title: {}", result_list[0].title);
//!     }
//! # }
//! ```
mod search;
use reqwest::Client as ReqClient;
use search::*;
use std::default::Default;
use tokio::runtime::Builder;

const AGENT: &str = "Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0";

/// # Examples
/// ```rust
/// use search_with_google::search;
/// # async fn run() {
/// let results = search("rust", 3, None).await;
/// if let Ok(result_list) = results {
///     println!("Title : {}\nLink : {}", result_list[0].title, result_list[0].link);
/// }
/// # }
/// ```
pub async fn search<T: Into<Option<u32>>, U: Into<Option<String>>>(
    query: &str,
    limit: T,
    agent: U,
) -> Result<Vec<SearchResult>, Error> {
    let limit: Option<u32> = limit.into();
    let agent: Option<String> = agent.into();

    google(
        query,
        limit.unwrap_or(10),
        agent.unwrap_or_else(|| AGENT.to_string()),
        None,
    )
    .await
}

///Returns an async client
///```
/// use search_with_google::Client;
/// # async fn run() {
/// let client = Client::default();
/// let results = client.search("rust", 3, None).await;
/// if let Ok(result_list) = results {
///     println!("Title : {}\nLink : {}", result_list[0].title, result_list[0].link);
/// }
/// # }
///```
pub struct Client {
    client: ReqClient,
}

impl Default for Client {
    fn default() -> Self {
        Client {
            client: ReqClient::new(),
        }
    }
}

impl Client {
    pub async fn search<T: Into<Option<u32>>, U: Into<Option<String>>>(
        &self,
        query: &str,
        limit: T,
        agent: U,
    ) -> Result<Vec<SearchResult>, Error> {
        let limit: Option<u32> = limit.into();
        let agent: Option<String> = agent.into();

        google(
            query,
            limit.unwrap_or(10),
            agent.unwrap_or_else(|| AGENT.to_string()),
            Some(&self.client),
        )
        .await
    }
}

pub mod blocking {
    use super::*;

    ///Returns a blocking client
    ///```
    /// use search_with_google::blocking::Client;
    ///let client = Client::default();
    /// let results = client.search("rust", 3, None);
    /// if let Ok(result_list) = results {
    ///     println!("Title : {}\nLink : {}", result_list[0].title, result_list[0].link);
    /// }
    ///```
    pub struct Client {
        client: ReqClient,
    }

    impl Default for Client {
        fn default() -> Self {
            Client {
                client: ReqClient::new(),
            }
        }
    }

    impl Client {
        pub fn search<T: Into<Option<u32>>, U: Into<Option<String>>>(
            &self,
            query: &str,
            limit: T,
            agent: U,
        ) -> Result<Vec<SearchResult>, Error> {
            let limit: Option<u32> = limit.into();
            let agent: Option<String> = agent.into();

            Builder::new()
                .basic_scheduler()
                .enable_all()
                .build()?
                .block_on(google(
                    query,
                    limit.unwrap_or(10),
                    agent.unwrap_or_else(|| AGENT.to_string()),
                    Some(&self.client),
                ))
        }
    }

    /// # Examples
    /// ```rust
    /// use search_with_google::blocking::search;
    /// let results = search("rust", 3, None);
    /// if let Ok(result_list) = results {
    ///     println!("Title : {}\nLink : {}", result_list[0].title, result_list[0].link);
    /// }
    /// ```
    pub fn search<T: Into<Option<u32>>, U: Into<Option<String>>>(
        query: &str,
        limit: T,
        agent: U,
    ) -> Result<Vec<SearchResult>, Error> {
        let limit: Option<u32> = limit.into();
        let agent: Option<String> = agent.into();
        Builder::new()
            .basic_scheduler()
            .enable_all()
            .build()?
            .block_on(google(
                query,
                limit.unwrap_or(10),
                agent.unwrap_or_else(|| AGENT.to_string()),
                None,
            ))
    }
}

#[cfg(test)]
mod static_tests {
    use super::*;
    #[test]
    fn search_test() {
        let results = blocking::search("rust", None, None);
        if let Ok(result_list) = results {
            assert!(!result_list[0].title.is_empty());
            assert!(!result_list[0].link.is_empty());
            assert!(!result_list[0].description.is_empty());
            assert!(!result_list[0].description_raw.is_empty());
        }
    }
    #[test]
    fn limit_test() {
        let results = blocking::search("minecraft", 4, None);
        if let Ok(result_list) = results {
            assert!(result_list.len() <= 4);
        }
    }
    #[test]
    fn search_test_async() {
        let results = tokio_test::block_on(search("rust", None, None));
        if let Ok(result_list) = results {
            assert!(!result_list[0].title.is_empty());
            assert!(!result_list[0].link.is_empty());
            assert!(!result_list[0].description.is_empty());
            assert!(!result_list[0].description_raw.is_empty());
        }
    }
}

#[cfg(test)]
mod client_tests {
    use super::*;
    #[test]
    fn search_test() {
        let client = blocking::Client::default();
        let results = client.search("rust", None, None);
        if let Ok(result_list) = results {
            assert!(!result_list[0].title.is_empty());
            assert!(!result_list[0].link.is_empty());
            assert!(!result_list[0].description.is_empty());
            assert!(!result_list[0].description_raw.is_empty());
        }
    }
    #[test]
    fn limit_test() {
        let client = blocking::Client::default();
        let results = client.search("minecraft", 4, None);
        if let Ok(result_list) = results {
            assert!(result_list.len() <= 4);
        }
    }
    #[test]
    fn search_test_async() {
        let client = Client::default();
        let results = tokio_test::block_on(client.search("rust", None, None));
        if let Ok(result_list) = results {
            assert!(!result_list[0].title.is_empty());
            assert!(!result_list[0].link.is_empty());
            assert!(!result_list[0].description.is_empty());
            assert!(!result_list[0].description_raw.is_empty());
        }
    }
}
